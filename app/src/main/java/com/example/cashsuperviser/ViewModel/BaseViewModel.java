package com.example.cashsuperviser.ViewModel;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.cashsuperviser.Application.AppApplication;
import com.example.cashsuperviser.DB.MoneyRepository;
import com.google.gson.Gson;

public class BaseViewModel extends AndroidViewModel {

    protected Gson gson;

    protected AppApplication appApplication;

    protected MoneyRepository moneyRepository;

    public BaseViewModel(@Nullable Application application) {
        super(application);
        gson = new Gson();
        appApplication = AppApplication.getInstance();
        moneyRepository = new MoneyRepository(application);
    }

    public int page_index = 0;

    public MutableLiveData<Integer> page_liveData = new MutableLiveData<>();

    public MutableLiveData<Integer> getPage_liveData() {
        return page_liveData;
    }

    public int getPage_index() {
        return page_index;
    }

    public void switchPage(int page) {
        if (page != 0) {
            page_index = page;
        }
        page_liveData.postValue(page);
    }

    public void back() {
        switch (page_index) {
            case 1:
                switchPage(0);
            case 2:
                switchPage(1);
                break;
            case 3:
                switchPage(0);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + page_index);
        }
    }

}

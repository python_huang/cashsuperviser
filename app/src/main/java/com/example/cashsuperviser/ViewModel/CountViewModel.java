package com.example.cashsuperviser.ViewModel;

import android.app.Application;

import androidx.annotation.Nullable;

import com.example.cashsuperviser.DB.MoneyDao;

import java.util.List;

public class CountViewModel extends BaseViewModel {

    private int income;
    private int outcome;

    public CountViewModel(@Nullable Application application) {
        super(application);
    }

    public MoneyDao getMoneyDao() {
        return moneyRepository.getMoneyDao();
    }

    public int getPosCash() {
        List<Integer> moneyList = getMoneyDao().getMoney();
        if (moneyList != null) {
            for (int i = 0; i < moneyList.size(); i++) {
                int cash = moneyList.get(i);
                if (cash >= 0) {
                    income += cash;
                }
            }
        }
        return income;
    }

    public int getNavCash() {
        List<Integer> moneyList = getMoneyDao().getMoney();
        if (moneyList != null) {
            for (int i = 0; i < moneyList.size(); i++) {
                int cash = moneyList.get(i);
                if (cash < 0) {
                    outcome += cash;
                }
            }
        }
        return outcome;
    }

    public int getAllCash() {
        return income + outcome;
    }

    public void resetCount() {
        income = 0;
        outcome = 0;
    }

}

package com.example.cashsuperviser.ViewModel;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.cashsuperviser.DB.MoneyRecord;

import java.util.ArrayList;
import java.util.List;

public class AllRecordViewModel extends BaseViewModel {

    private boolean selected_mode = false;

    private List<MoneyRecord> moneyRecordList = new ArrayList<>();

    private List<MoneyRecord> selected_list = new ArrayList<>();

    public AllRecordViewModel(@Nullable Application application) {
        super(application);
    }

    public void setMoneyRecordList(List<MoneyRecord> moneyRecordList) {
        this.moneyRecordList = moneyRecordList;
    }

    public void clearMoneyRecordList() {
        moneyRecordList.clear();
    }

    public List<MoneyRecord> getMoneyRecordList() {
        return moneyRecordList;
    }

    public void addSelected_list(MoneyRecord selected) {
        selected_list.add(selected);
    }

    public void removeSelected_list(MoneyRecord selected) {
        selected_list.remove(selected);
    }

    public void clearSelected_list() {
        selected_list.clear();
    }

    public List<MoneyRecord> getSelected_list() {
        return selected_list;
    }

    public void setSelected_mode(boolean state) {
        selected_mode = state;
    }

    public boolean getSelected_mode() {
        return selected_mode;
    }

}

package com.example.cashsuperviser.ViewModel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.cashsuperviser.DB.MoneyDao;
import com.example.cashsuperviser.DB.MoneyRecord;
import com.example.cashsuperviser.DB.MoneyRepository;

import java.util.List;

public class MainViewModel extends BaseViewModel {

    public MainViewModel(@Nullable Application application) {
        super(application);
    }

    public void insertData(MoneyRecord moneyRecord) {
        moneyRepository.insert(moneyRecord);
    }

    public void deleteData(MoneyRecord moneyRecord) {
        moneyRepository.delete(moneyRecord);
    }

    public void deleteData(List<MoneyRecord> moneyRecordList) {
        moneyRepository.delete(moneyRecordList);
    }

    public LiveData<List<MoneyRecord>> get_LiveMoneyRecordLists() {
        return moneyRepository.getLive_ListMoneyRecord();
    }

    public List<MoneyRecord> get_selectedList(boolean select) {
        return moneyRepository.getSelect(select);
    }

    public List<MoneyRecord> getYear(int year) {
        return moneyRepository.getYear(year);
    }

    public List<MoneyRecord> getMonth(int month) {
        if (month == 0) {
            return moneyRepository.getAll();
        } else {
            return moneyRepository.getMonth(month);
        }
    }

    public List<MoneyRecord> getYearAndMonth(int year, int month) {
        return moneyRepository.getYearAndMonth(year, month);
    }

}

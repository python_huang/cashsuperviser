package com.example.cashsuperviser.ViewModel;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class FilterViewModel extends BaseViewModel {

    private MutableLiveData<Integer> mutableLiveData_month = new MutableLiveData<>();

    public FilterViewModel(@Nullable Application application) {
        super(application);
    }

    public void setMutableLiveData_month(int month) {
        mutableLiveData_month.postValue(month);
    }

    public MutableLiveData<Integer> getMutableLiveData_month() {
        return mutableLiveData_month;
    }

}

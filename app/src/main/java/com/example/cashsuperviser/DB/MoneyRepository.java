package com.example.cashsuperviser.DB;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class MoneyRepository {

    private MoneyDao moneyDao;

    public MoneyRepository(Application application) {
        AppDataBase db = AppDataBase.getDatabase(application);
        moneyDao = db.moneyDao();
    }

    public MoneyDao getMoneyDao() {
        return moneyDao;
    }

    public LiveData<List<MoneyRecord>> getLive_ListMoneyRecord() {
        return moneyDao.getAll_LiveData();
    }

    public List<MoneyRecord> getSelect(boolean select) {
        return moneyDao.getSelected(select);
    }

    public void insert(MoneyRecord data) {
        new MoneyRepository.insertAsyncTask(moneyDao).execute(data);
    }

    public void insert(List<MoneyRecord> datas) {
        new MoneyRepository.insertsAsyncTask(moneyDao).execute(datas);
    }

    public void delete(MoneyRecord data) {
        new MoneyRepository.deleteAsyncTask(moneyDao).execute(data);
    }

    public void delete(List<MoneyRecord> datas) {
        new MoneyRepository.deletesAsyncTask(moneyDao).execute(datas);
    }

    public void deleteAll() {
        new MoneyRepository.deleteAllAsyncTask(moneyDao).execute();
    }

    public List<MoneyRecord> getAll() {
        return moneyDao.getAll();
    }

    public List<MoneyRecord> getYear(int year) {
        return moneyDao.getYear(year);
    }

    public List<MoneyRecord> getMonth(int month) {
        return moneyDao.getMonth(month);
    }

    public List<MoneyRecord> getYearAndMonth(int year, int month) {
        return moneyDao.getYearAndMonth(year, month);
    }

    private static class insertAsyncTask extends AsyncTask<MoneyRecord, Void, Void> {

        private MoneyDao mAsyncTaskDao;

        insertAsyncTask(MoneyDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final MoneyRecord... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class insertsAsyncTask extends AsyncTask<List<MoneyRecord>, Void, Void> {

        private MoneyDao mAsyncTaskDao;

        insertsAsyncTask(MoneyDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final List<MoneyRecord>... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class deleteAllAsyncTask extends AsyncTask<Void, Void, Void> {

        private MoneyDao mAsyncTaskDao;

        deleteAllAsyncTask(MoneyDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Void... voids) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<MoneyRecord, Void, Void> {

        private MoneyDao mAsyncTaskDao;

        deleteAsyncTask(MoneyDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final MoneyRecord... params) {
            mAsyncTaskDao.delete(params[0]);
            return null;
        }
    }

    private static class deletesAsyncTask extends AsyncTask<List<MoneyRecord>, Void, Void> {

        private MoneyDao mAsyncTaskDao;

        deletesAsyncTask(MoneyDao dao) {
            mAsyncTaskDao = dao;
        }

        @SafeVarargs
        @Override
        protected final Void doInBackground(final List<MoneyRecord>... lists) {
            List<MoneyRecord>[] copy_list = lists;
            for (List<MoneyRecord> list : copy_list) {
                mAsyncTaskDao.delete(list);
            }
            return null;
        }
    }

}

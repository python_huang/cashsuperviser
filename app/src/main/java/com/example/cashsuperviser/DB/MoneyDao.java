package com.example.cashsuperviser.DB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface MoneyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(MoneyRecord moneyRecord);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<MoneyRecord> datas);

    @Query("DELETE FROM Data")
    void deleteAll();

    @Delete
    void delete(MoneyRecord moneyRecord);

    @Delete
    void delete(List<MoneyRecord> datas);

    @Query("Select * from Data")
    LiveData<List<MoneyRecord>> getAll_LiveData();

    @Query("Select * from Data")
    List<MoneyRecord> getAll();

    @Query("Select * from Data Where year = :y")
    List<MoneyRecord> getYear(int y);

    @Query("Select * from Data Where month = :m")
    List<MoneyRecord> getMonth(int m);

    @Query("Select * from Data Where year = :yyyy and month = :mm")
    List<MoneyRecord> getYearAndMonth(int yyyy, int mm);

    @Query("Select money from Data")
    List<Integer> getMoney();

    @Query("Select * from Data Where `select` = :select")
    List<MoneyRecord> getSelected(boolean select);

}
package com.example.cashsuperviser.DB;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Data")
public class MoneyRecord {
    @PrimaryKey(autoGenerate = true)

    @ColumnInfo(name = "id")
    public int id;
    @ColumnInfo(name = "year")
    public int year;
    @ColumnInfo(name = "month")
    public int month;
    @ColumnInfo(name = "day")
    public int day;
    @ColumnInfo(name = "description")
    public String description;
    @ColumnInfo(name = "type")
    public String type;
    @ColumnInfo(name = "money")
    public int money;
    @ColumnInfo(name = "select")
    public boolean select;

    public MoneyRecord(int year, int month, int day, String description, String type, int money, boolean select) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.description = description;
        this.type = type;
        this.money = money;
        this.select = select;
    }

    public void setYear(int Year) {
        this.year = Year;
    }

    public void setMonth(int Month) {
        this.month = month;
    }

    public void setDay(int Day) {
        this.day = Day;
    }

    public void setDescription(String Description) {
        this.description = Description;
    }

    public void setType(String Type) {
        this.type = Type;
    }

    public void setMoney(int Money) {
        this.money = Money;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    public int getMoney() {
        return money;
    }

    public boolean getSelect() {
        return select;
    }

}

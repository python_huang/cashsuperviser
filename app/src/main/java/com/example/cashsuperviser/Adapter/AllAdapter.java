package com.example.cashsuperviser.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cashsuperviser.DB.MoneyRecord;
import com.example.cashsuperviser.R;

import java.util.ArrayList;
import java.util.List;

public class AllAdapter extends RecyclerView.Adapter<AllAdapter.ViewHolder> {

    private final LayoutInflater layoutInflater;

    private final AllClickListener allClickListener;

    private Context context;

    private List<MoneyRecord> moneyRecordList = new ArrayList<>();

    public AllAdapter(Context context, AllClickListener allClickListener) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.allClickListener = allClickListener;
    }

    @NonNull
    @Override
    public AllAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.layout_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AllAdapter.ViewHolder holder, int position) {

        int id = position;
        int leave = id % 2;

        MoneyRecord item = moneyRecordList.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                allClickListener.onClick(v, position);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                allClickListener.onLongClick(v, position);
                return true;
            }
        });

        String year_data = String.valueOf(item.getYear());
        String month_data = String.valueOf(item.getMonth());
        String day_data = String.valueOf(item.getDay());
        String date_data = month_data + " / " + day_data;
        String description_data = item.getDescription();
        String type_data = item.getType();
        String money_data = String.valueOf(item.getMoney());

        holder.year.setText(year_data);
        holder.date.setText(date_data);
        holder.description.setText(description_data);
        holder.type.setText(type_data);
        holder.money.setText(money_data);

        if (item.getSelect()) {
            holder.img.setVisibility(View.VISIBLE);
        } else {
            holder.img.setVisibility(View.INVISIBLE);
        }

        if (id == 0) {
            holder.view.setBackgroundColor(context.getResources().getColor(R.color.lightpink));
        } else if (leave != 0) {
            holder.view.setBackgroundColor(context.getResources().getColor(R.color.aliceblue));
        } else if (leave == 0) {
            holder.view.setBackgroundColor(context.getResources().getColor(R.color.lightpink));
        }

    }

    public void setMoneyRecordList(List<MoneyRecord> moneyRecordList) {
        this.moneyRecordList = moneyRecordList;
        notifyDataSetChanged();
    }

    public List<MoneyRecord> getMoneyRecordList() {
        return moneyRecordList;
    }

    @Override
    public int getItemCount() {
        Log.e("Item_number", String.valueOf(moneyRecordList.size()));
        return moneyRecordList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView img;

        private final View view;

        private final TextView year;
        private final TextView date;
        private final TextView description;
        private final TextView type;
        private final TextView money;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView.findViewById(R.id.item_view);
            img = itemView.findViewById(R.id.item_img);
            year = itemView.findViewById(R.id.item_year);
            date = itemView.findViewById(R.id.item_mmdd);
            description = itemView.findViewById(R.id.item_description);
            type = itemView.findViewById(R.id.item_type);
            money = itemView.findViewById(R.id.item_money);
        }
    }

    public interface AllClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

}

package com.example.cashsuperviser.Activities;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.cashsuperviser.R;
import com.google.gson.Gson;

public abstract class BaseActivity extends AppCompatActivity {

    protected AlertDialog message_dialog = null;

    protected Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupView();
        init();
    }

    public abstract void setupView();

    public abstract void init();

    protected void requestPermission(String permission, int request_code) {
        String[] permisssion = new String[]{permission};
        ActivityCompat.requestPermissions(this, permisssion, request_code);
    }

    protected void requestPermission(String[] permissions, int request_code) {
        ActivityCompat.requestPermissions(this, permissions, request_code);
    }

    protected boolean hasPermission(String permission) {
        return ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    protected void initMessageDialog(String msg) {
        dismissDialog();
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View view = layoutInflater.inflate(R.layout.layout_message_dialog, null);
        TextView message = view.findViewById(R.id.message);
        message.setText(msg);
        Button check_btn = view.findViewById(R.id.message_button);
        check_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageCheck();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setView(view);
        message_dialog = builder.create();
        message_dialog.show();
    }

    protected void messageCheck() {
        dismissDialog();
    }

    protected void dismissDialog() {
        if (message_dialog != null) {
            if (message_dialog.isShowing()) {
                message_dialog.dismiss();
            }
        }
    }

    @Override
    protected void onDestroy() {
        // dismiss dialog or popup window avoid memory leak
        dismissDialog();
        super.onDestroy();
    }

}

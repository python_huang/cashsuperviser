package com.example.cashsuperviser.Activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cashsuperviser.Adapter.AllAdapter;
import com.example.cashsuperviser.DB.MoneyRecord;
import com.example.cashsuperviser.R;
import com.example.cashsuperviser.ViewModel.AllRecordViewModel;
import com.example.cashsuperviser.ViewModel.MainViewModel;

import java.util.ArrayList;
import java.util.List;

public class AllRecordActivity extends BaseActivity {

    private ImageView back, add, delete;
    private RecyclerView recyclerView;
    private AllAdapter allAdapter;

    private MainViewModel mainViewModel;
    private AllRecordViewModel allRecordViewModel;

    private List<MoneyRecord> allMoneyRecord = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_all_list);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setupView() {
        back = findViewById(R.id.all_list_back_btn);
        add = findViewById(R.id.all_list_add_btn);
        delete = findViewById(R.id.all_list_delete_btn);
        recyclerView = findViewById(R.id.all_list_recyclerview);

        back.setOnClickListener(viewClickListener);
        add.setOnClickListener(viewClickListener);
        delete.setOnClickListener(viewClickListener);

        normal_view();
    }

    @Override
    public void init() {
        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        allRecordViewModel = new ViewModelProvider(this).get(AllRecordViewModel.class);

        allAdapter = new AllAdapter(this, new AllAdapter.AllClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (allRecordViewModel.getSelected_mode()) {
                    if (allMoneyRecord.get(position).getSelect()) {
                        allMoneyRecord.get(position).setSelect(false);
                        allRecordViewModel.removeSelected_list(allMoneyRecord.get(position));
                    } else {
                        allRecordViewModel.addSelected_list(allMoneyRecord.get(position));
                        allMoneyRecord.get(position).setSelect(true);
                    }
                    Log.e("Item_Selected", gson.toJson(allMoneyRecord.get(position)));
                    allAdapter.notifyDataSetChanged();
                    if (allRecordViewModel.getSelected_list().size() == 0) {
                        allRecordViewModel.setSelected_mode(false);
                        normal_view();
                    }
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                if (!allRecordViewModel.getSelected_mode()) {
                    allRecordViewModel.addSelected_list(allMoneyRecord.get(position));
                    allRecordViewModel.setSelected_mode(true);
                    allMoneyRecord.get(position).setSelect(true);
                    Log.e("Item_Selected", gson.toJson(allMoneyRecord.get(position)));
                    selected_view();
                    allAdapter.notifyDataSetChanged();
                }
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(allAdapter);

        observerData();
    }

    private void observerData() {
        mainViewModel.get_LiveMoneyRecordLists().observe(this, new Observer<List<MoneyRecord>>() {
            @Override
            public void onChanged(List<MoneyRecord> moneyRecordList) {
                Log.e("LiveMoneyRecord", "資料庫變更");
                allAdapter.setMoneyRecordList(moneyRecordList);
                allAdapter.notifyDataSetChanged();
                allMoneyRecord = moneyRecordList;
            }
        });

        mainViewModel.getPage_liveData().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if (integer != null) {
                    if (integer == 1) {
                        allMoneyRecord = allAdapter.getMoneyRecordList();
                    } else {

                    }
                }
            }
        });
    }

    private final View.OnClickListener viewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.all_list_back_btn:
                    if (!allRecordViewModel.getSelected_mode()) {
                        backIntent();
                    }
                    break;
                case R.id.all_list_add_btn:
                    if (!allRecordViewModel.getSelected_mode()) {
                        addIntent();
                    }
                    break;
                case R.id.all_list_delete_btn:
                    if (allRecordViewModel.getSelected_mode()) {
                        deleteIntent();
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private void backIntent() {
        mainViewModel.switchPage(0);
        Intent intent = new Intent();
        intent.setClass(AllRecordActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void addIntent() {
        mainViewModel.switchPage(2);
        Intent intent = new Intent();
        intent.setClass(AllRecordActivity.this, AddActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void deleteIntent() {
        mainViewModel.deleteData(allRecordViewModel.getSelected_list());
        allRecordViewModel.setSelected_mode(false);
        normal_view();

        allRecordViewModel.clearSelected_list();
    }

    private void normal_view() {
        back.setVisibility(View.VISIBLE);
        add.setVisibility(View.VISIBLE);
        delete.setVisibility(View.INVISIBLE);
    }

    private void selected_view() {
        back.setVisibility(View.INVISIBLE);
        add.setVisibility(View.INVISIBLE);
        delete.setVisibility(View.VISIBLE);
    }

}

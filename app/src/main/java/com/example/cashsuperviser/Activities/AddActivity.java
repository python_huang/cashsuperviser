package com.example.cashsuperviser.Activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.cashsuperviser.DB.MoneyRecord;
import com.example.cashsuperviser.R;
import com.example.cashsuperviser.ViewModel.MainViewModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AddActivity extends BaseActivity {

    private EditText date, type, description, money;
    private ImageView back_btn;
    private Button ok_btn;

    private MainViewModel mainViewModel;

    private Calendar calendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_add_list);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setupView() {
        date = findViewById(R.id.add_date);
        type = findViewById(R.id.add_type);
        description = findViewById(R.id.add_description);
        money = findViewById(R.id.add_money);
        ok_btn = findViewById(R.id.add_ok);
        back_btn = findViewById(R.id.add_back_btn);

        date.setOnClickListener(viewClickListener);
        type.setOnClickListener(viewClickListener);
        ok_btn.setOnClickListener(viewClickListener);
        back_btn.setOnClickListener(viewClickListener);
    }

    @Override
    public void init() {
        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);

        description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        money.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        observerData();
    }

    private void observerData() {
        mainViewModel.getPage_liveData().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if (integer != null) {
                    if (integer == 2) {

                    }
                }
            }
        });
    }

    private View.OnClickListener viewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.add_date:
                    dateIntent();
                    break;
                case R.id.add_type:
                    typeIntent();
                    break;
                case R.id.add_ok:
                    okClick();
                    break;
                case R.id.add_back_btn:
                    backIntent();
                    break;
                default:
                    break;
            }
        }
    };

    private void dateIntent() {
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        SimpleDateFormat format = new SimpleDateFormat("YYYY / MM / dd");

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, DatePickerDialog.THEME_HOLO_LIGHT,
                (view, year1, month1, dayOfMonth) -> {
                    calendar.set(year1, month1, dayOfMonth);
                    String date_text = format.format(calendar.getTime());
                    date.setText(date_text);
                }, year, month, day);
        datePickerDialog.show();
    }

    private void typeIntent() {
        String[] list = getResources().getStringArray(R.array.typeArray);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("金額類型");
        builder.setSingleChoiceItems(list, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                type.setText(list[which]);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = builder.show();
        alertDialog.create();
    }

    private synchronized void okIntent() {
        String time_text = date.getText().toString();
        String description_text = description.getText().toString();
        String type_text = type.getText().toString();
        String money_text = money.getText().toString();

        int add_year = Integer.parseInt(time_text.substring(0, 4));
        int add_month = Integer.parseInt(time_text.substring(7, 9));
        int add_day = Integer.parseInt(time_text.substring(12, 14));

        int cash = Integer.parseInt(money_text);
        if (add_year != 0) {
            if (!type_text.equals("-")) {
                if (!money_text.equals("-")) {
                    if (type_text.equals("收入")) {
                        MoneyRecord moneyRecord = new MoneyRecord(add_year, add_month, add_day, description_text, type_text, cash, false);
                        Log.e("加入資料庫-收入", gson.toJson(moneyRecord));
                        mainViewModel.insertData(moneyRecord);
                    } else if (type_text.equals("支出")) {
                        MoneyRecord moneyRecord = new MoneyRecord(add_year, add_month, add_day, description_text, type_text, -cash, false);
                        Log.e("加入資料庫-支出", gson.toJson(moneyRecord));
                        mainViewModel.insertData(moneyRecord);
                    }
                }
            }
        }

        mainViewModel.switchPage(1);

        Intent intent = new Intent();
        intent.setClass(AddActivity.this, AllRecordActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void okClick() {
        String x = date.getText().toString();
        String y = type.getText().toString();
        String z = money.getText().toString();
        if (x.equals("") || y.equals("") || z.equals("")) {
            Toast.makeText(this, "請輸入資料", Toast.LENGTH_SHORT).show();
        } else {
            okIntent();
        }
    }

    private void backIntent() {
        Intent intent = new Intent();
        intent.setClass(AddActivity.this, AllRecordActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

}

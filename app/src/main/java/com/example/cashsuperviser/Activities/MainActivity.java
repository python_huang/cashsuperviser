package com.example.cashsuperviser.Activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.cashsuperviser.DB.MoneyRecord;
import com.example.cashsuperviser.R;
import com.example.cashsuperviser.ViewModel.CountViewModel;
import com.example.cashsuperviser.ViewModel.MainViewModel;

import java.util.List;

public class MainActivity extends BaseActivity {

    private MainViewModel mainViewModel;
    private CountViewModel countViewModel;

    private TextView income, outcome, count;

    private Button all_record, filter_record;



    @Override
    protected synchronized void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
    }

    @Override
    public synchronized void setupView() {
        income = findViewById(R.id.main_income);
        outcome = findViewById(R.id.main_outcome);
        count = findViewById(R.id.main_count);
        all_record = findViewById(R.id.main_all_record);
        filter_record = findViewById(R.id.main_filter_record);

        all_record.setOnClickListener(viewClickListener);
        filter_record.setOnClickListener(viewClickListener);
    }

    @Override
    public synchronized void init() {
        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        countViewModel = new ViewModelProvider(this).get(CountViewModel.class);

        observerData();
    }

    private void observerData() {
        mainViewModel.getPage_liveData().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if (integer != null) {
                    if (integer == 0) {

                    }
                }
            }
        });

        mainViewModel.get_LiveMoneyRecordLists().observe(this, new Observer<List<MoneyRecord>>() {
            @Override
            public void onChanged(List<MoneyRecord> moneyRecordList) {
                if (moneyRecordList != null) {
                    String income_text = String.valueOf(countViewModel.getPosCash());
                    String outcome_text = String.valueOf(countViewModel.getNavCash());
                    String count_text = String.valueOf(countViewModel.getAllCash());
                    income.setText(income_text);
                    outcome.setText(outcome_text);
                    count.setText(count_text);
                    countViewModel.resetCount();
                }
            }
        });



    }

    private View.OnClickListener viewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.main_all_record:
                    allRecordIntent();
                    break;
                case R.id.main_filter_record:
                    filterRecordIntent();
                    break;
                default:
                    break;
            }
        }
    };

    private void allRecordIntent() {
        mainViewModel.switchPage(1);
        Intent intent = new Intent();
        intent.setClass(MainActivity.this, AllRecordActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void filterRecordIntent() {
        mainViewModel.switchPage(3);
        Intent intent = new Intent();
        intent.setClass(MainActivity.this, FilterRecordActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

}

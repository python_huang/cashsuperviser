package com.example.cashsuperviser.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cashsuperviser.Adapter.FilterAdapter;
import com.example.cashsuperviser.DB.MoneyRecord;
import com.example.cashsuperviser.R;
import com.example.cashsuperviser.ViewModel.MainViewModel;
import com.example.cashsuperviser.ViewModel.FilterViewModel;

import java.util.List;

public class FilterRecordActivity extends BaseActivity {

    private ImageView back_btn;
    private EditText month;
    private RecyclerView recyclerView;
    private FilterAdapter filterAdapter;

    private MainViewModel mainViewModel;
    private FilterViewModel filterViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_filter_list);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setupView() {
        back_btn = findViewById(R.id.filter_list_back_btn);
        month = findViewById(R.id.filter_month);
        recyclerView = findViewById(R.id.filter_recyclerview);

        back_btn.setOnClickListener(viewClickListener);
        month.setOnClickListener(viewClickListener);
    }

    @Override
    public void init() {
        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        filterViewModel = new ViewModelProvider(this).get(FilterViewModel.class);

        filterAdapter = new FilterAdapter(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(filterAdapter);

        observerData();
    }

    private void observerData() {
        mainViewModel.getPage_liveData().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if (integer != null) {
                    if (integer == 3) {

                    }
                }
            }
        });

        mainViewModel.get_LiveMoneyRecordLists().observe(this, new Observer<List<MoneyRecord>>() {
            @Override
            public void onChanged(List<MoneyRecord> moneyRecordList) {
                Log.e("LiveMoneyRecord", "資料庫變更");
                filterAdapter.setMoneyRecordList(moneyRecordList);
                filterAdapter.notifyDataSetChanged();
            }
        });

        filterViewModel.getMutableLiveData_month().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                int month = integer;
                filterAdapter.clearMoneyRecordList();
                filterAdapter.setMoneyRecordList(mainViewModel.getMonth(month));
                filterAdapter.notifyDataSetChanged();
            }
        });

    }

    private View.OnClickListener viewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.filter_month:
                    monthIntent();
                    break;
                case R.id.filter_list_back_btn:
                    backIntent();
                    break;
                default:
                    break;
            }
        }
    };

    private void monthIntent() {
        String[] list = getResources().getStringArray(R.array.monthArray);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("顯示月份");
        builder.setSingleChoiceItems(list, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                month.setText(list[which]);
                int month_int = Integer.parseInt(list[which]);
                filterViewModel.setMutableLiveData_month(month_int);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = builder.show();
        alertDialog.create();
    }

    private void backIntent() {
        mainViewModel.switchPage(0);
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

}

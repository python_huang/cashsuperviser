package com.example.cashsuperviser.Application;

import android.app.Application;

public class AppApplication extends Application {

    private static AppApplication Instance = null;

    public static synchronized AppApplication getInstance() {
        return Instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Instance = this;
    }

}
